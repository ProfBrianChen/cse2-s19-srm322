import java.util.*;

public class Area{
   public static void main(String args[]){
       String shape;
       int h = 0;
       double length, width, radius, base, height;
       System.out.print("Enter a shape(rectangle, triangle, or circle): ");
       Scanner sc = new Scanner(System.in);
       do { h = 0;
           shape = sc.next();
           
           if (shape.equals("rectangle")) {
               System.out.print("Enter a length : ");
               length = sc.nextDouble();
             
               System.out.print("Enter a width : ");
               width = sc.nextDouble();
             
               if (length >= 0 && width >= 0) {
                   Area.rectangle(length,width);
               }
             else {
                   System.out.println("");  
                   System.out.println("Invalid inputs please try again");
               }
           }
           
           else if (shape.equals("triangle")) {
               System.out.print("Enter a height : ");
               height = sc.nextDouble();
                 
               System.out.print("Enter a base : ");
               base = sc.nextDouble();
             
               if (height >= 0 && base >= 0) {
                   Area.triangle(height,base);
               }
             else {
               System.out.println("");  
               System.out.println("Invalid inputs please try again");
               }
           }
           
           else if (shape.equals("circle")) {
               System.out.print("Enter a radius : ");
               radius = sc.nextDouble();
             
               if (radius >= 0 ){
                   Area.circle(radius);
               }
             else {
                   System.out.println("");  
                   System.out.println("Invalid inputs please try again");
               }
           }
           
           else {
             System.out.println("");  
             System.out.print("Enter valid shape (rectangle, triangle, or circle) : ");
               h = 1;
           }
       }
     
     while (h != 0);
   }
  
   public static void rectangle(double length,double width) {
       System.out.println("");    
       System.out.println("Area of the rectangle : " + length*width);
   }
  
   public static void triangle(double height,double base) {
       System.out.println("");  
       System.out.println("Area of the triangle : " + 0.5*(height*base));
   }
  
   public static void circle(double radius) {
       System.out.println("");  
       System.out.println("Area of the circle : " + 3.14*radius*radius);
   }
}

