import java.util.Scanner;

public class Convert {
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the distance in meters: ");
    int meters = myScanner.nextInt();
    
    double inchpermeter = 39.3701;
    
    System.out.println(meters + " meters is " + (meters/inchpermeter) + " inches" );  
  }
}