import java.util.Random;
import java.util.Scanner;

public class CardGenerator {
  public static void main(String[] args){
    
    Scanner newScanner = new Scanner(System.in);
    String Cards[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
    String HDCS[] = {"Hearts", "Diamonds", "Clubs", "Spades"};
    //Strings determine card number or suit
    Random random = new Random();
    
   System.out.println("You have selected the " + Cards[random.nextInt(Cards.length)] +" of " + HDCS[random.nextInt(HDCS.length)]);
  }
}