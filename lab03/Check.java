import java.util.Scanner;

public class Check{
public static void main(String[] args){
Scanner myScanner = new Scanner(System.in);

System.out.print("Enter original Check Price in the form xx.xx: ");
double checkCost = myScanner.nextDouble();

System.out.print("Enter the percentage tip that you wish to apply as a whole number in the form xx: ");
double tipPercent = myScanner.nextDouble();
tipPercent /=100;

System.out.print("Enter number of participants: ");
int numPeople = myScanner.nextInt();

double totalCost;
double costPerPerson;
int dollars, dimes, pennies;
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;

dollars = (int)costPerPerson;
dimes = (int)(costPerPerson * 10) % 10;
pennies = (int)(costPerPerson * 100) % 10;

System.out.println("Each person owes $" + dollars + '.' + dimes + pennies);
}
}