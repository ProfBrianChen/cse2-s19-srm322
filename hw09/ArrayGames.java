import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class ArrayGames {
   public static int binarySearch(int arr[], int l, int r, int x) {
       if (r >= l) {

           int mid = l + (r - l) / 2;
           if (arr[mid] == x) {
               return mid;
           }

           if (arr[mid] > x)
               return binarySearch(arr, l, mid - 1, x);

           return binarySearch(arr, mid + 1, r, x);
       }
       return -1;
   }

   private static int linearSearch(int arr[], int n, int ele) {
       int i;
       for (i = 0; i < n; i++) {
           if (arr[i] == ele)
               return i;
       }
       return -1;
   }

   public static int[] fillRandomNumbers(int n) {
       int arr[] = new int[n];
       Random r = new Random();
       for (int i = 0; i < n; i++)
           arr[i] = r.nextInt(n);
       return arr;
   }

   public static int[] fillRandomAscNumbers(int n) {
       int arr[] = new int[n];
       Random r = new Random();
       int temp = n;
       for (int i = 0; i < n; i++) {
           temp = r.nextInt(n) + temp+1;
           arr[i] = temp;
       }
       return arr;
   }

   public static void main(String args[]) {
       Scanner sc = new Scanner(System.in);
       System.out.println("Input 1 for Linear Search or 2 for Binary Search");
       int ch = sc.nextInt();
       System.out.println("Enter number: ");
       int n = sc.nextInt();
       int arr[] = null;
       if (ch == 1) {
           arr = fillRandomNumbers(n);
           printArray(arr);
           System.out.println("Enter a number shown to search position");
           int ele = sc.nextInt();
           int pos = linearSearch(arr, arr.length, ele);
           if (pos != -1)
               System.out.println(ele + " found at : " + pos);
           else
               System.out.println(ele + " does not exist in array");
       } else {
           arr = fillRandomAscNumbers(n);
           printArray(arr);
           System.out.println("Enter number to search");
           int ele = sc.nextInt();
           int pos = binarySearch(arr, 0, arr.length, ele);
           if (pos != -1)
               System.out.println(ele + " found at : " + pos);
           else
               System.out.println(ele + " does not exist in array");
       }
   }

   private static void printArray(int[] aArr) {

       for (int i : aArr)
           System.out.print(i + " ");
       System.out.println();
   }
}