import java.util.Arrays;
import java.util.Random;


public class Lab08 {

    private static Random random = new Random();

    public static void main(String[] args) {

        int[] numbers = new int[50 + random.nextInt(51)];
        System.out.println("Size of the array: " + numbers.length);
        
      for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(100);
     
            System.out.println(numbers[i]);  //Print out the contents of the array.
        }
        int range = getRange(numbers);
        
      System.out.println("Range   : " + range);
        double mean = getMean(numbers);
       
      System.out.println("Mean    : " + String.format("%.2f", mean));
        double stdDev = getStdDev(numbers, mean);
      
      System.out.println("Std Dev : " + String.format("%.2f", stdDev));
        shuffle(numbers);
    }

    public static int getRange(int[] n) {
        Arrays.sort(n);

        return n[n.length - 1] - n[0];
    }

   
    public static double getMean(int[] n) {   //Step 4
        double sum = 0;
        for (int i : n) sum += i;
        return sum / n.length;
    }

    
    public static double getStdDev(int[] n, double mean) {  //Step 5a

        double squareSum = 0;
        for (int i : n) squareSum += (mean - i) * (mean - i);
        return squareSum / (n.length - 1);   }

    
 public static void shuffle(int[] n) {   //Step 6a


        for (int i = 0; i < n.length; i++) {
            int indexOne = random.nextInt(n.length);
            int indexTwo = random.nextInt(n.length);
            int temp = n[indexOne];
            n[indexOne] = n[indexTwo];
            n[indexTwo] = temp;
            System.out.print("[" + i + "]" + n[i]);
        }
    }
}