public class PokerHandCheck {
  //main method
  public static void main(String[] args) {
    boolean pairFound = false;
    String[] suits = {"Clubs", "Hearts", "Spades", "Diamonds"};
    String[] cards = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
   
    int rSuit0 = (int) (Math.random()*4);
    int rCard0 = (int) (Math.random()*13);
   
    int rSuit1 = (int) (Math.random()*4);
    int rCard1 = (int) (Math.random()*13);
   
    int rSuit2 = (int) (Math.random()*4);
    int rCard2 = (int) (Math.random()*13);
   
    int rSuit3 = (int) (Math.random()*4);
    int rCard3 = (int) (Math.random()*13);
   
    int rSuit4 = (int) (Math.random()*4);
    int rCard4 = (int) (Math.random()*13);
   
    String card0 = cards[rCard0] +" of "+ suits[rSuit0];
    String card1 = cards[rCard1] +" of "+ suits[rSuit1];
    String card2 = cards[rCard2] +" of "+ suits[rSuit2];
    String card3 = cards[rCard3] +" of "+ suits[rSuit3];
    String card4 = cards[rCard4] +" of "+ suits[rSuit4];
   
   
   
   
    System.out.println("You picked:");
    System.out.println("The "+card0);
    System.out.println("The "+card1);
    System.out.println("The "+card2);
    System.out.println("The "+card3);
    System.out.println("The "+card4);
   
    // three of a kind based off of first card
    if (rCard0 == rCard1 && rCard0 == rCard2 && rCard1 == rCard2 && rSuit0 == rSuit1 && rSuit0 == rSuit2 && rSuit1 == rSuit2) {
      System.out.println("Three of a kind");
    } else if (rCard0 == rCard2 && rCard0 == rCard3 && rCard2 == rCard3 && rSuit0 == rSuit2 && rSuit0 == rSuit3 && rSuit2 == rSuit3) {
       System.out.println("Three of a kind");
    } else if (rCard0 == rCard3 && rCard0 == rCard4 && rCard3 == rCard4 && rSuit0 == rSuit3 && rSuit0 == rSuit4 && rSuit3 == rSuit4) {
       System.out.println("Three of a kind");
    } else if (rCard0 == rCard1 && rCard0 == rCard4 && rCard1 == rCard4 && rSuit0 == rSuit1 && rSuit0 == rSuit4 && rSuit1 == rSuit4) {
       System.out.println("Three of a kind");
    } else if (rCard0 == rCard2 && rCard0 == rCard4 && rCard2 == rCard4 && rSuit0 == rSuit2 && rSuit0 == rSuit4 && rSuit2 == rSuit4) {
       System.out.println("Three of a kind");
    } else if (rCard0 == rCard1 && rCard0 == rCard3 && rCard1 == rCard3 && rSuit0 == rSuit1 && rSuit0 == rSuit3 && rSuit1 == rSuit3) {
       System.out.println("Three of a kind");
    } else if (rCard0 == rCard2 && rCard0 == rCard3 && rCard2 == rCard3 && rSuit0 == rSuit2 && rSuit0 == rSuit3 && rSuit2 == rSuit3) {
       System.out.println("Three of a kind");
    }
   
    if (rCard1 == rCard2 && rCard1 == rCard3 && rCard2 == rCard3 && rSuit1 == rSuit2 && rSuit1 == rSuit3 && rSuit2 == rSuit3) {
      System.out.println("Three of a kind");
    } else if (rCard1 == rCard3 && rCard1 == rCard4 && rCard3 == rCard4 && rSuit1 == rSuit3 && rSuit1 == rSuit4 && rSuit3 == rSuit4) {
      System.out.println("Three of a kind");
    } else if (rCard1 == rCard2 && rCard1 == rCard4 && rCard2 == rCard4 && rSuit1 == rSuit2 && rSuit1 == rSuit4 && rSuit2 == rSuit4) {
      System.out.println("Three of a kind");
    }
   
    // two pair
    if (!pairFound) {
      if (rCard0 == rCard1 && rCard0 == rCard2 && rSuit0 == rSuit1 && rSuit0 == rSuit2) {
      System.out.println("two pair");
    } else if (rCard0 == rCard2 && rCard0 == rCard3 && rSuit0 == rSuit2 && rSuit0 == rSuit3) {
       System.out.println("two pair");
    } else if (rCard0 == rCard3 && rCard0 == rCard4 && rSuit0 == rSuit3 && rSuit0 == rSuit4) {
       System.out.println("two pair");
    } else if (rCard0 == rCard1 && rCard0 == rCard4 && rSuit0 == rSuit1 && rSuit0 == rSuit4) {
       System.out.println("two pair");
    } else if (rCard0 == rCard2 && rCard0 == rCard4 && rSuit0 == rSuit2 && rSuit0 == rSuit4) {
       System.out.println("two pair");
    } else if (rCard0 == rCard1 && rCard0 == rCard3 && rSuit0 == rSuit1 && rSuit0 == rSuit3) {
       System.out.println("two pair");
    } else if (rCard0 == rCard2 && rCard0 == rCard3 && rSuit0 == rSuit2 && rSuit0 == rSuit3) {
       System.out.println("two pair");
    }
   
    if (rCard1 == rCard2 && rCard1 == rCard3 && rSuit1 == rSuit2 && rSuit1 == rSuit3) {
      System.out.println("two pair");
    } else if (rCard1 == rCard3 && rCard1 == rCard4 && rSuit1 == rSuit3 && rSuit1 == rSuit4) {
      System.out.println("two pair");
    } else if (rCard1 == rCard2 && rCard1 == rCard4 && rSuit1 == rSuit2 && rSuit1 == rSuit4) {
      System.out.println("two pair");
    }
      else {
        System.out.println("high card");
      }
      
      
    }
  }// end main method
}// end class